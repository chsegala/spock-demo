package com.ihm.spock.spockdemo;

import java.time.LocalDateTime;

public class CsvSanitizerService {

    public LocalDateTime sanitizeInput(LocalDateTime input) {
        Integer seconds = input.getSecond();
        if(seconds.equals(0)){
            return input.minusSeconds(1);
        }
        return input;
    }
}
