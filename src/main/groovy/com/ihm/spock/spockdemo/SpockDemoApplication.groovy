package com.ihm.spock.spockdemo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpockDemoApplication {

	static void main(String[] args) {
		SpringApplication.run SpockDemoApplication, args
	}
}
