package com.ihm.spock.spockdemo

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime

import static java.time.LocalDateTime.parse

class CsvSanitizerSpec extends Specification {
    @Unroll
    def "assert that sanitized #input should be #output"() {
        given: "the sanitizer service"
        def service = new CsvSanitizerService()

        when: "sanitation is run"
        def sanitizedHour = service.sanitizeInput(input)

        then: "expects"
        output == sanitizedHour

        where:
        input                        | output
        parse("2017-05-01T23:29:59") | parse("2017-05-01T23:29:59")
        parse("2017-05-01T23:30:00") | parse("2017-05-01T23:29:59")
        parse("2017-05-01T23:30:01") | parse("2017-05-01T23:30:01")
        parse("2017-05-01T23:59:59") | parse("2017-05-01T23:59:59")
        parse("2017-05-02T00:00:00") | parse("2017-05-01T23:59:59")
    }

    @Unroll
    def "mock with template example"(){
        given:
        def t = Mock(CsvSanitizerService) {
            sanitizeInput(_ as LocalDateTime) >> {
                return parse("2017-05-01T23:29:59")
            }
        }

        when:
        def result = t.sanitizeInput(LocalDateTime.now())

        then:
        result == parse("2017-05-01T23:29:59")
    }
}
